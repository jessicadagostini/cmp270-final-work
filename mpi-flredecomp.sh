mpiexec \
    -x PATH=$PATH \
    -x SCOREP_ENABLE_TRACING=true \
    -x SCOREP_TOTAL_MEMORY=$SCOREP_TOTAL_MEMORY \
    -x SCOREP_MPI_ENABLE_GROUPS=$SCOREP_MPI_ENABLE_GROUPS \
    -machinefile lista-de-recursos.txt \
    -n $NPROCS \
    ${BINPREFIXFLREDECOMP} /INSTALL/bin/flredecomp \
    -i 1 -o $NPROCS -v -l flow_past_sphere_Re100 flow_past_sphere_Re100_flredecomp
