#!/bin/bash

source $HOME/spack/share/spack/setup-env.sh
export PATH=$(spack location -i scorep@6.0%gcc@8.3.0)/bin:$PATH

KEY=flow-past-sphere
export BINPREFIXFLUIDITY=$HOME/containers-fluidity/fluidity-completed-instrumented.sif 
export BINPREFIXFLREDECOMP=$HOME/containers-fluidity/flredecomp-completed-instrumented.sif 

EXAMPLEPATH=$HOME/containers-fluidity/fluidity/examples/flow_past_sphere_Re100
FLREDECOMP=$HOME/containers-fluidity/mpi-flredecomp.sh
FLUIDITY=$HOME/containers-fluidity/mpi-fluidity.sh
export SCOREP_ENABLE_TRACING=true
export SCOREP_TOTAL_MEMORY=4GB
export SCOREP_MPI_ENABLE_GROUPS=coll,env,err,ext,io,p2p,misc,perf,rma,topo,type,xnonblock,xreqtest

    export NPROCS=8

    EXPPATH=$HOME/containers-fluidity/$KEY-exec
    mkdir -p $EXPPATH
    cp -r $EXAMPLEPATH/* $EXPPATH
    cp -r $FLUIDITY $EXPPATH
    cp -r $FLREDECOMP $EXPPATH

    pushd $EXPPATH/
    {
	  if [ $NPROCS != 1 ]; then
	      echo "Running FLREDECOMP"
	      bash ./mpi-flredecomp.sh
	  fi
	  echo "Converting otf2csv"
        cd scorep-2020*
	  $HOME/otf2utils/otf22csv traces.otf2 > traces-flredecomp.csv
	  gzip traces-flredecomp.csv
	  cd ..
	  rm -r scorep-2020*
	  
	  echo "Running FLUIDITY"
	  if [ $NPROCS != 1 ]; then
	      echo "PARALLEL"
	      start=`date +%s.%N`
	      bash ./mpi-fluidity.sh
	      end=`date +%s.%N`
	  fi
        RUNTIME=$(bc <<< "scale=2;$end-$start")
        echo "Runtime ${NPROCS},${RUNTIME}"
	  echo "Converting otf2csv"
        cd scorep-2020*
	  $HOME/otf2utils/otf22csv traces.otf2 > traces-fluidity.csv
	  gzip traces-fluidity.csv
    }
    popd
