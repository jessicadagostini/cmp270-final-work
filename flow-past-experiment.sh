#!/bin/bash

KEY=flow-past-sphere

export BINPREFIXFLUIDITY=$HOME/fluidity-original.sif 

EXAMPLEPATH=$HOME/fluidity/examples/flow_past_sphere_Re100
PROJETO=$HOME/projeto-experimental-flow.csv
FLREDECOMP=$HOME/mpi-flredecomp.sh
FLUIDITY=$HOME/mpi-fluidity.sh

tail -n +2 $PROJETO |
while IFS=, read -r name runnoinstro runno runnodstr NPROCS REPETITION
do
    export REPETITION=$(echo $REPETITION | sed 's/\"//g')
    export NPROCS=$(echo $NPROCS | sed 's/\"//g')

    EXPPATH=$HOME/$KEY/speedup/exec-$REPETITI0ON-$NPROCS
    mkdir -p $EXPPATH
    cp -r $EXAMPLEPATH/* $EXPPATH
    cp -r $FLUIDITY $EXPPATH
    cp -r $FLREDECOMP $EXPPATH

    pushd $EXPPATH/
    {
	      
	      if [ $NPROCS != 1 ]; then
		  echo "Running FLREDECOMP"
		  bash ./mpi-flredecomp.sh
	      fi
	      echo "Running FLUIDITY"
	      if [ $NPROCS != 1 ]; then
		  echo "PARALLEL"
		  start=`date +%s.%N`
		  bash ./mpi-fluidity.sh
		  end=`date +%s.%N`
	      else
		  echo "SINGLE"
		  start=`date +%s.%N`
		  bash ./sing-fluidity.sh
		  end=`date +%s.%N`
	      fi
        RUNTIME=$(bc <<< "scale=2;$end-$start")
        echo "Runtime ${REPETITION},${NPROCS},${RUNTIME}"
        echo "Removing unnecessary files and folders"
	      rm -rf *.ele *.edge *.face *.node *.halo *.poly *vtu *.stat *.log-* matrixdump* flow_past_sphere_Re100_? flow_past_sphere_Re100_?? first_timestep_adapted_mesh* Sphere_drag.pdf
    }
    popd
    sleep 30
done
