mpirun \
    -x PATH=$PATH \
    -x SCOREP_ENABLE_TRACING=true \
    -x SCOREP_TOTAL_MEMORY=$SCOREP_TOTAL_MEMORY \
    -x SCOREP_MPI_ENABLE_GROUPS=$SCOREP_MPI_ENABLE_GROUPS \
    -machinefile lista-de-recursos.txt \
    -n $NPROCS \
    ${BINPREFIXFLUIDITY} /INSTALL/bin/fluidity \
    -v2 -l backward_facing_step_3d_flredecomp.flml
