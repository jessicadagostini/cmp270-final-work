#!/bin/bash

KEY=flow-past-sphere
export BINPREFIXFLUIDITY=$HOME/fluidity-hpctoolkit.sif 

EXAMPLEPATH=$HOME/fluidity/examples/flow_past_sphere_Re100
FLREDECOMP=$HOME/mpi-flredecomp.sh
FLUIDITY=$HOME/mpi-fluidity.sh
spack load hpctoolkit

PROJETO=(8)

for NPROCS in "${PROJETO[@]}";
do
    export NPROCS=$NPROCS

    EXPPATH=$HOME/$KEY/hpctoolkit/exec-$NPROCS
    mkdir -p $EXPPATH
    cp -r $EXAMPLEPATH/* $EXPPATH
    cp -r $FLUIDITY $EXPPATH
    cp -r $FLREDECOMP $EXPPATH

    pushd $EXPPATH/
    {
	  if [ $NPROCS != 1 ]; then
	      echo "Running FLREDECOMP"
	      bash ./mpi-flredecomp.sh
	  fi
	  
echo "Running FLUIDITY"
	  if [ $NPROCS != 1 ]; then
	      echo "PARALLEL"
	      start=`date +%s.%N`
	      bash ./mpi-fluidity.sh
	      end=`date +%s.%N`
	  fi
        RUNTIME=$(bc <<< "scale=2;$end-$start")
        echo "Runtime ${NPROCS},${RUNTIME}"
	  }
    popd
done
