#rm -rf fluidity-c6c026d649036753807f54ba7fb6debb77f12aea
unzip -o fluidity-c6c026d649036753807f54ba7fb6debb77f12aea.zip
cd fluidity-c6c026d649036753807f54ba7fb6debb77f12aea
# Fix compilation errors
sed -i '1i\extern "C"{\n#include <unistd.h>\n}' ./tools/*_main.cpp
# Install
INSTALL_PREFIX=/INSTALL
CPPFLAGS=-I${INSTALL_PREFIX}/include \
LDFLAGS=-L${INSTALL_PREFIX}/lib \
MPICC=hpclink \
MPICXX=hpclink \
MPIF90=hpclink \ 
CC=hpclink \
./configure \
    --enable-vtk \
    --prefix=${INSTALL_PREFIX}
# Update makefile to include scorep on compilation
make all
make tools
make install
