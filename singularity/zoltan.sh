#rm -rf Zoltan_v3.8/
tar --warning=no-unknown-keyword -zvxf zoltan_distrib_v3.8.tar.gz
cd Zoltan_v3.8/
# this fixes a problem with the perl evaluation complaining
sed -i -e '13,15d' ./config/generate-makeoptions.pl
# build dir
mkdir -p Zoltan-build
cd Zoltan-build
# where it should be installed
INSTALL_PREFIX=/INSTALL
../configure \
    x86_64-linux-gnu \
    --prefix=${INSTALL_PREFIX} \
    --enable-mpi \
    --with-mpi-compilers \
    --enable-f90interface \
    --disable-examples \
    --enable-zoltan-cppdriver \
    --with-parmetis \
    --with-parmetis-incdir=${INSTALL_PREFIX}/include \
    --with-parmetis-libdir=${INSTALL_PREFIX}/lib
make everything
make install
