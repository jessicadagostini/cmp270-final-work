#rm -rf ParMetis-3.1.1
tar -zvxf ParMetis-3.1.1.tar.gz
cd ParMetis-3.1.1
make
export INSTALL_PREFIX=/INSTALL
mkdir -p ${INSTALL_PREFIX}/lib ${INSTALL_PREFIX}/include/
cp lib*.a ${INSTALL_PREFIX}/lib/
cp parmetis.h ${INSTALL_PREFIX}/include/
