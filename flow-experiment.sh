#!/bin/bash

KEY=flow-past-sphere
export BINPREFIXFLUIDITY=$HOME/fluidity-completed-instrumented.sif 

EXAMPLEPATH=$HOME/fluidity/examples/flow_past_sphere_Re100
FLREDECOMP=$HOME/mpi-flredecomp.sh
FLUIDITY=$HOME/mpi-fluidity.sh
export SCOREP_ENABLE_TRACING=true
export SCOREP_TOTAL_MEMORY=4GB
export SCOREP_MPI_ENABLE_GROUPS=coll,env,err,ext,io,p2p,misc,perf,rma,topo,type,xnonblock,xreqtest

PROJETO=(8 16 32 64 128 256)

for NPROCS in "${PROJETO[@]}";
do
    export NPROCS=$NPROCS

    EXPPATH=$HOME/$KEY/all-instrumentation/exec-$NPROCS
    mkdir -p $EXPPATH
    cp -r $EXAMPLEPATH/* $EXPPATH
    cp -r $FLUIDITY $EXPPATH
    cp -r $FLREDECOMP $EXPPATH

    pushd $EXPPATH/
    {
	  if [ $NPROCS != 1 ]; then
	      echo "Running FLREDECOMP"
	      bash ./mpi-flredecomp.sh
	  fi
	  echo "Converting otf2csv"
        cd scorep-2020*
	  $HOME/otf2utils/otf22csv traces.otf2 > traces.csv
	  echo "Keep only my instrumented regions"
	  cat traces.csv | grep -v -e "MPI_" > traces-flredecomp-min.csv
	  mv traces-flredecomp-min.csv ../
	  cd ..
	  rm -r scorep-2020*
	  
	  echo "Running FLUIDITY"
	  if [ $NPROCS != 1 ]; then
	      echo "PARALLEL"
	      start=`date +%s.%N`
	      bash ./mpi-fluidity.sh
	      end=`date +%s.%N`
	  fi
        RUNTIME=$(bc <<< "scale=2;$end-$start")
        echo "Runtime ${NPROCS},${RUNTIME}"
	  echo "Converting otf2csv"
        cd scorep-2020*
	  $HOME/otf2utils/otf22csv traces.otf2 > traces.csv
	  echo "Keep only my instrumented regions"
	  cat traces.csv | grep -v -e "MPI_" > traces-fluidity-min.csv
	  mv traces-fluidity-min.csv ../
	  cd ..
	  rm -r scorep-2020*
	  sleep 30
    }
    popd
done
