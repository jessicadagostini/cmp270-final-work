FROM amd64/debian:buster

RUN apt-get update && apt-get upgrade

RUN useradd -ms /bin/bash cmp270

RUN apt install -y git gcc g++ gfortran libopenmpi3

RUN apt install -y libvtk7-dev 

RUN apt install -y libscotchparmetis-dev petsc-dev libhdf5-dev

RUN apt install -y libnetcdf-dev libnetcdff-dev

RUN apt install -y cmake curl

RUN apt install -y wget nano

USER cmp270

WORKDIR /home/cmp270

RUN git clone https://github.com/FluidityProject/fluidity.git

RUN git clone https://github.com/spack/spack.git

#RUN /bin/bash -c "source spack/share/spack/setup-env.sh" && spack install zoltan ^openmpi@3.1.3 && spack install gmsh@2.16.0 ^openmpi@3.1.3
