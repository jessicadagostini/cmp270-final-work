mpiexec \ 
    -machinefile $OAR_NODE_FILE \
    -n $NPROCS \
    --mca orte_rsh_agent "oarsh" -- /grid5000/code/bin/singularity exec \
    ${BINPREFIXFLUIDITY} /INSTALL/bin/flredecomp \
    -i 1 -o $NPROCS -v -l flow_past_sphere_Re100 flow_past_sphere_Re100_flredecomp > /dev/null < /dev/null
