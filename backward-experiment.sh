#!/bin/bash

source $HOME/spack/share/spack/setup-env.sh
export PATH=$(spack location -i scorep@6.0%gcc@8.3.0)/bin:$PATH
export PATH=$PATH:$(spack location -i gmsh@2.16.0)/bin:
export PATH=$HOME/otf2utils/:$PATH

KEY=backward-facing-step
export BINPREFIXFLUIDITY=$HOME/containers-fluidity/fluidity-completed-instrumented.sif 
export BINPREFIXFLREDECOMP=$HOME/containers-fluidity/flredecomp-completed-instrumented.sif

EXAMPLEPATH=$HOME/containers-fluidity/fluidity/examples/backward_facing_step_3d
FLREDECOMP=$HOME/containers-fluidity/mpi-flredecomp-back.sh
FLUIDITY=$HOME/containers-fluidity/mpi-fluidity-back.sh
export SCOREP_ENABLE_TRACING=true
export SCOREP_TOTAL_MEMORY=4GB
export SCOREP_MPI_ENABLE_GROUPS=coll,env,err,ext,io,p2p,misc,perf,rma,topo,type,xnonblock,xreqtest

    export NPROCS=8

    EXPPATH=$HOME/containers-fluidity/$KEY-exec
    mkdir -p $EXPPATH
    cp -r $EXAMPLEPATH/* $EXPPATH
    cp -r $FLUIDITY $EXPPATH
    cp -r $FLREDECOMP $EXPPATH

    pushd $EXPPATH/
    {
	  echo "Running preprocess"
	  gmsh -3 -optimize -o step3d.msh src/step3d.geo
    
	  if [ $NPROCS != 1 ]; then
	      echo "Running FLREDECOMP"
	      bash ./mpi-flredecomp-back.sh
	  fi
	  echo "Converting otf2csv"
	  cd scorep-2020*
	  $HOME/otf2utils/otf22csv traces.otf2 > traces.csv
	  gzip traces-flredecomp.csv
	  mv traces-flredecomp.csv.gz ../
	  cd ..
	  rm -r scorep-2020*
	  echo "Running FLUIDITY"
	  if [ $NPROCS != 1 ]; then
	      echo "PARALLEL"
	      start=`date +%s.%N`
	      bash ./mpi-fluidity-back.sh
	      end=`date +%s.%N`
	  fi
        RUNTIME=$(bc <<< "scale=2;$end-$start")
        echo "Runtime ${NPROCS},${RUNTIME}"
	  echo "Converting otf2csv"
        cd scorep-2020*
	  $HOME/otf2utils/otf22csv traces.otf2 > traces.csv
	  gzip traces-fluidity.csv
	  mv traces-fluidity.csv.gz ../
	  cd ..
	  rm -r scorep-2020*
    }
    popd
