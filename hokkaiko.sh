#!/bin/bash

# Load SPACK and put mpirun in the PATH
source $HOME/spack/share/spack/setup-env.sh
export PATH=$(spack location -i scorep@6.0%gcc@8.3.0)/bin:$PATH
export PATH=$PATH:$(spack location -i gmsh@2.16.0)/bin:
export PATH=$HOME/otf2utils/:$PATH

export BINPREFIXFLUIDITY=$HOME/cmp270-final-work/singularity/fluidity.sif 
export BINPREFIXFLREDECOMP=$HOME/cmp270-final-work/singularity/flredecomp.sif
export SCOREP_ENABLE_TRACING=true
export SCOREP_TOTAL_MEMORY=4GB
export NPROCS=2

EXPPATH=$HOME/fluidity/examples/hokkaido-nansei-oki_tsunami

pushd $EXPPATH/
{
    start=`date +%s.%N`

    #make clean
    rm -r scorep*
    echo "Running preprocess"
    cd mesh && gmsh -bin MonaiValley_C.geo -2 && cd ..
    echo "Running FLREDECOMP"
    mpiexec \
        -x SCOREP_ENABLE_TRACING=true \
	-x SCOREP_TOTAL_MEMORY=$SCOREP_TOTAL_MEMORY \
        -np $NPROCS \
        ${BINPREFIXFLREDECOMP} -i 1 -o $NPROCS -v -l MonaiValley_C_p1p1_nu0.01_kmkstab_drag0.002_butcircularoundisland0.2 MonaiValley_C_p1p1_nu0.01_kmkstab_drag0.002_butcircularoundisland0.2_flredecomp
    echo "Running FLUIDITY"
    mpirun \
        -x SCOREP_ENABLE_TRACING=true \
	-x SCOREP_TOTAL_MEMORY=$SCOREP_TOTAL_MEMORY \
        -n $NPROCS \
        ${BINPREFIXFLUIDITY} -v2 -l MonaiValley_C_p1p1_nu0.01_kmkstab_drag0.002_butcircularoundisland0.2_flredecomp.flml
    cd scorep-*
    otf22csv traces.otf2 > traces.csv
    gzip traces.csv

    end=`date +%s.%N`
    echo "($end - $start)" | bc -l
}
popd
