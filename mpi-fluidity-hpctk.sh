mpirun \
    hpcrun \
    -machinefile $OAR_NODE_FILE \
    -n $NPROCS \
    --mca orte_rsh_agent "oarsh" -- /grid5000/code/bin/singularity exec \
    ${BINPREFIXFLUIDITY} /INSTALL/bin/fluidity \
    -v2 -l flow_past_sphere_Re100_flredecomp.flml > /dev/null < /dev/null
